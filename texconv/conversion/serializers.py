from rest_framework import serializers

from texconv.conversion.models import TeXDocument, ConvertedDocument, \
    ConversionLog


class TeXDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeXDocument
        fields = '__all__'


class ConvertedDocumentSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)

        request = self.context.get('request')
        if instance.img:
            img_relative_url = instance.img.url
            data['img'] = request.build_absolute_uri(img_relative_url)

        data['status'] = instance.get_status_display()
        return data

    class Meta:
        model = ConvertedDocument
        fields = ('id', 'status', 'img')


class ConversionLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConversionLog
        fields = '__all__'
