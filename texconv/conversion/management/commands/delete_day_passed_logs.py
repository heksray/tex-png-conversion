from datetime import timedelta

from django.core.management import BaseCommand
from django.utils import timezone

from texconv.conversion.models import ConversionLog


LOG_LIFETIME_DAYS = 1


class Command(BaseCommand):
    help = 'Удалить прошлодневные логи'

    def handle(self, *args, **options):
        limit_date = timezone.now() - timedelta(days=LOG_LIFETIME_DAYS)
        ConversionLog.objects.filter(creation_datetime__lte=limit_date)
