from django.db import models


class TeXDocument(models.Model):
    text = models.TextField(
        verbose_name='Текст',
        help_text='Текст документа в формате TeX',
    )

    class Meta:
        verbose_name = 'TeX документ'
        verbose_name_plural = 'TeX документы'


class ConvertedDocument(models.Model):
    CONVERTED = 'conv'
    ERROR = 'err'
    IN_PROGRESS = 'in_pr'
    NOT_STARTED = 'n_st'

    STATUS_CHOICES = (
        (CONVERTED, 'Документ конвертирован'),
        (ERROR, 'Ошибка при конвертации'),
        (IN_PROGRESS, 'Конвертирование еще не завершено'),
        (NOT_STARTED, 'Конвертирование еще не началось'),
    )

    origin = models.ForeignKey(
        TeXDocument, related_name='converted',
    )
    status = models.CharField(
        choices=STATUS_CHOICES, max_length=5, default=NOT_STARTED,
        verbose_name='Статус конвертирования',
    )
    img = models.ImageField(
        verbose_name='Картинка', blank=True, null=True,
        help_text='Картинка, в которую был конвертирован исходный документ',
    )

    class Meta:
        verbose_name = 'Ковертированный документ'
        verbose_name_plural = 'Конвертированные документы'


class ConversionLog(models.Model):
    log = models.TextField(
        verbose_name='Лог конвертирования',
    )
    converted_document = models.OneToOneField(
        ConvertedDocument, verbose_name='Конвертированный документ',
    )
    creation_datetime = models.DateTimeField(
        verbose_name='Дата создания', auto_now_add=True
    )

    class Meta:
        verbose_name = 'Лог конвертирования'
        verbose_name_plural = 'Логи конвертирования'
