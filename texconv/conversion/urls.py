from django.conf.urls import url

from texconv.conversion import views

urlpatterns = [
    url(r'^documents/$', views.TeXDocumentConversionView.as_view()),
    url(r'^documents/(?P<pk>[0-9]+)/$', views.ConvertedDocumentView.as_view()),
]
