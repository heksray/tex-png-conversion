from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from texconv.conversion.models import ConvertedDocument
from texconv.conversion.serializers import TeXDocumentSerializer, \
    ConvertedDocumentSerializer
from texconv.conversion.tasks import convert_tex_to_img


class TeXDocumentConversionView(APIView):
    @transaction.atomic()
    def post(self, request):
        serializer = TeXDocumentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        tex_document = serializer.save()

        converted_document = ConvertedDocument.objects.create(
            origin=tex_document
        )

        convert_tex_to_img.apply_async(
            args=[converted_document.pk, tex_document.text]
        )

        response_data = {
            'id': converted_document.pk
        }

        return Response(status=status.HTTP_200_OK, data=response_data)


class ConvertedDocumentView(APIView):
    def get(self, request, pk):
        converted_document = get_object_or_404(ConvertedDocument, pk=pk)

        serializer_context = {
            'request': self.request
        }
        serializer = ConvertedDocumentSerializer(
            converted_document, **{'context': serializer_context}
        )
        return Response(status=status.HTTP_200_OK, data=serializer.data)
