from texconv.celery import app
from texconv.tex_to_png_conversion.lib import convert_tex_to_png


@app.task
def convert_tex_to_img(converted_document_id, tex_text):
    convert_tex_to_png(converted_document_id, tex_text)
