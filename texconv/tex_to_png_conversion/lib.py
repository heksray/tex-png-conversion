import os
import shutil
import subprocess

from django.conf import settings
from texconv.conversion.models import ConvertedDocument, ConversionLog
from wand.color import Color
from wand.image import Image


IMAGE_HIGH_RESOLUTION = 300
CONVERTED_DOCUMENTS_DIR_BASENAME = 'converted'
CONVERTED_DOCUMENTS_DIR_NAME = os.path.join(settings.MEDIA_ROOT,
                                            CONVERTED_DOCUMENTS_DIR_BASENAME)


def create_conversion_log_model(logfile_name, converted_document):
    with open(logfile_name, 'r') as log:
        log_text = log.read()

    ConversionLog.objects.create(
        converted_document=converted_document, log=log_text
    )


def convert_tex_to_png(converted_document_id, tex_text):
    tmp_dir_name = '{0}/{1}'.format(settings.TMP_DIR, converted_document_id)
    if not os.path.isdir(tmp_dir_name):
        os.makedirs(tmp_dir_name)

    converted_document = ConvertedDocument.objects.get(pk=converted_document_id)
    converted_document.status = ConvertedDocument.IN_PROGRESS
    converted_document.save()

    tex_file_name = '{0}/{1}.tex'.format(tmp_dir_name, converted_document_id)
    with open(tex_file_name, 'w') as tex_file:
        tex_file.write(tex_text)

    logfile_name = '{0}/{1}.log'.format(tmp_dir_name, converted_document_id)
    pdf_file_name = '{0}/{1}.pdf'.format(tmp_dir_name, converted_document_id)

    try:
        subprocess.check_output([
            'pdflatex', '-interaction', 'nonstopmode',
            '-output-directory', tmp_dir_name, tex_file_name
        ])
        create_conversion_log_model(logfile_name, converted_document)
    except subprocess.CalledProcessError:
        # В CentOS нельзя ориентироваться на exit code комманды pdflatex,
        # даже при успешной генерации pdf exit code может быть не равен 0 из-за
        #  ворнингов
        # Из-за этого добавил явную проверку, создался ли pdf-file
        if not os.path.isfile(pdf_file_name):
            create_conversion_log_model(logfile_name, converted_document)
            converted_document.status = ConvertedDocument.ERROR
            converted_document.save()
            shutil.rmtree(tmp_dir_name)
            return

    if not os.path.isdir(CONVERTED_DOCUMENTS_DIR_NAME):
        os.makedirs(CONVERTED_DOCUMENTS_DIR_NAME)

    png_file_name = '{0}/{1}.png'.format(
        CONVERTED_DOCUMENTS_DIR_NAME, converted_document_id
    )

    with Image(filename=pdf_file_name, resolution=IMAGE_HIGH_RESOLUTION) as img:
        img.background_color = Color('white')
        img.alpha_channel = 'remove'
        img.save(filename=png_file_name)

    converted_document.img = '{0}/{1}'.format(
        CONVERTED_DOCUMENTS_DIR_BASENAME, os.path.basename(png_file_name)
    )
    converted_document.status = ConvertedDocument.CONVERTED
    converted_document.save()

    shutil.rmtree(tmp_dir_name)
