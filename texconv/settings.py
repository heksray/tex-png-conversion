# -*- coding: utf-8 -*-
import json
import os

# from evlogger.django import add_file_logger


BASE_DIR = os.path.dirname(os.path.dirname(
    os.path.abspath(__file__)
))

TMP_DIR = os.path.join(BASE_DIR, 'tmp')
if not os.path.isdir(TMP_DIR):
    os.makedirs(TMP_DIR)

# Application definition
INSTALLED_APPS = (
    # Приложения auth и contenttypes не предполагается использовать в данный
    # момент, но они нужны, чтобы не было Warning-ов при запуске проекта
    'django.contrib.auth',
    'django.contrib.contenttypes',

    'texconv.conversion',
)

ROOT_URLCONF = 'texconv.urls'

TEMPLATES = []

WSGI_APPLICATION = 'texconv.wsgi.application'

LANGUAGE_CODE = 'ru'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = False
USE_TZ = True

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

STATIC_URL = '/static/'

# Загрузка настроек из конфигурационных файлов
config_path = os.environ.get(
    'SP_VERIFIER_CONFIG', '/etc/spacel/texconv/django.conf')
if os.path.exists(config_path):
    config = json.loads(config_path)

    DEBUG = config['DEBUG']
    ALLOWED_HOSTS = config['ALLOWED_HOSTS']
    DATABASES = config['DATABASES']
    SECRET_KEY = config['SECRET_KEY']
    CACHES = config['CACHES']
    LOGGING = config['LOGGING']

    # add_file_logger(LOGGING, 'sp_feed.log')
try:
    from local_settings import *
except ImportError:
    pass
else:
    INSTALLED_APPS = list(INSTALLED_APPS) + EXT_INSTALLED_APPS
    # add_file_logger(LOGGING, 'tmp/sp_feed.log', path=BASE_DIR, level='DEBUG')
