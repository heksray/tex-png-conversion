import os
from setuptools import setup


with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name='texconv',
    version='0.1',
    packages=['texconv'],
    license='SPACEL License',
    description='Spacel document converter',
    long_description=README,
    url='',
    author='Nikolay Budaragin',
    author_email='nvlbud@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
    ],
    extras_require={
        'dev': [
            'ipython',
            'readline',
            'django-extensions',
            'Werkzeug',
            'ipython',
            'coverage',
        ],
    },
    install_requires=[
        'pip>=7.1',
        'Django==1.10',
        'psycopg2==2.6.2',
        'djangorestframework==3.4.5',
        'raven==5.9.0',
        'celery==4.0.2',
        'Pillow==4.1.1',
        'Wand==0.4.4',
    ],
    include_package_data=True,
)
