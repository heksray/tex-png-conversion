DEBUG = True

ALLOWED_HOSTS = ['*']

SECRET_KEY = 'change-me'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'sentry': {
            'level': 'DEBUG',
            'class': 'raven.contrib.django.handlers.SentryHandler',
        },
    },
    'loggers': {
        'django.request': {
            'level': 'WARNING',
            'handlers': ['sentry'],
            'propagate': True,
        },
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'texconv',
        'USER': 'spacel3',
        'PASSWORD': 'spacel3',
        'HOST': 'localhost',
        'PORT': 5432,

        'TEST': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'texconv',
            'USER': 'spacel3',
            'PASSWORD': 'spacel3',
            'HOST': 'localhost',
            'PORT': 5432,
        }
    },
}

EXT_INSTALLED_APPS = [
    'django_extensions',
    'django.contrib.staticfiles',
    'rest_framework',
]
