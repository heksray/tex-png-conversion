PROJECT_NAME = textconv
SOURCE_DIR = `dirname $0`
UPDATE_LOCK_FILE = .update.lock
PORT = 20007
PRIVATE_REPO = spacel


# Dev commands

dev:
	@echo "Installing dependencies"
	pip install -e .[dev] --upgrade
	@echo ""

shell_plus:
	./manage.py shell_plus
sp: shell_plus

runserver_plus:
	# Disable Werkzeug PIN code confirmation
	WERKZEUG_DEBUG_PIN="off" ./manage.py runserver_plus ${PORT}
rp: runserver_plus

runserver:
	./manage.py runserver ${PORT}
r: runserver

pip-upload:
	python setup.py sdist -v upload -r ${PRIVATE_REPO}


# Helpers

migrate:
	./manage.py migrate ${app} ${rev} ${fake}

makemigrations:
	./manage.py makemigrations ${app}

migrations: makemigrations

shell:
	./manage.py shell


# Production commands

git-update:
	@echo "Updating project"
	export LC_ALL=en_US.utf8
	@echo "Fetching source from git"
	git fetch
	touch ${UPDATE_LOCK_FILE}
	git checkout origin/master
	rm ${UPDATE_LOCK_FILE} > /dev/null

deploy: git-update migrate



.PHONY: dev pip-upload deploy git-update shell shell_plus sp runserver_plus rp runserver r makemigrations migrations
